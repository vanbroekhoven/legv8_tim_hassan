/* main.c simple program to test assembler program */

#include <stdio.h>


extern unsigned long long int multiply ( unsigned int a, unsigned int b);

int main(void)
{
	long long int answer = multiply(2 , 2); // vul hier a en b
	printf("answer is: = %lld\n", answer);

	return 0;

}


/*

#include <stdio.h>

extern long long int test(long long int a, long long int b);

int main(void)
{
    long long int a = test(3, 5);
    printf("Result of test(3, 5) = %lld\n", a);
    return 0;
}

	.globl test
test:
	add	X0, X0, X1
	br	X30
*/
