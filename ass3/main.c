/* main.c simple program to test assembler program */

#include <stdio.h>


void gnomeSort(long long int a[], size_t n);


int main(void)
{
	long long int a[] = {1, -2, 7, -4, 5};
	long long int b[] = {-4, -2, 1, 5, 7};

	gnomeSort (a, sizeof (a)/ sizeof (a [0]) );

	return 0;

}
