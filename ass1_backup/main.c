/* main.c simple program to test assembler program */

#include <stdio.h>


extern unsigned long long int test ( unsigned int a, unsigned int b);

int main(void)
{
	long long int answer = test(4294967295 , 4294967295); // vul hier a en b
	printf("answer is: = %llu\n", answer);

	return 0;

}
